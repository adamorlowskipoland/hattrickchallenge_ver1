/**
 * Created by MacOrlov on 05.07.2017.
 */

const model = {
    "timeProperties": {
        "startingSeasonDate": new Date("August 12, 2017 16:00:00"),
        "timeLeft": function () {
            var now = new Date();
            return this.startingSeasonDate - now;
        },
        "secondsToSeasonStart": function () {
            return Math.floor((this.timeLeft()) / 1000);
        },
        "minutesToSeasonStart": function () {
            return  Math.floor(this.secondsToSeasonStart() / 60);
        },
        "hoursToSeasonStart": function () {
            return Math.floor(this.minutesToSeasonStart() / 60);
        },
        "daysToSeasonStart": function () {
            return Math.floor(this.hoursToSeasonStart() / 24);
        }
    },
    "chartProperties": {
        "diameter": 140,
        "lineWidth": 5,
        "rotate": 360
    },
    "charts": {
        "days": document.getElementById('days'),
        "hours": document.getElementById('hours'),
        "minutes": document.getElementById('minutes'),
        "seconds": document.getElementById('seconds')
    }
};

const operator = {
    "hoursInADayLeft": function () {
        return model.timeProperties.hoursToSeasonStart() - (model.timeProperties.daysToSeasonStart() * 24);
    },
    "minutesInAnHourLeft": function () {
        return model.timeProperties.minutesToSeasonStart() - (model.timeProperties.hoursToSeasonStart() * 60);
    },
    "secondsInAMinuteLeft": function () {
        return model.timeProperties.secondsToSeasonStart() - (model.timeProperties.minutesToSeasonStart() * 60);
    },
    "putTimeLeftToCharts": function () {
        var daysToSeasonStart = model.timeProperties.daysToSeasonStart();
        var hoursInADayLeft = operator.hoursInADayLeft();
        var minutesInAnHourLeft = operator.minutesInAnHourLeft();
        var secondsInAMinuteLeft = operator.secondsInAMinuteLeft();
        model.charts.days.innerHTML = "days: <br>" + daysToSeasonStart;
        model.charts.hours.innerHTML = "hours: <br>" + hoursInADayLeft;
        model.charts.minutes.innerHTML = "minutes: <br>" + minutesInAnHourLeft;
        model.charts.seconds.innerHTML = "seconds: <br>" + secondsInAMinuteLeft;
    },
    "canvasCircleForSecAndMin": function (givenTime) {
        return ((6.28 / 60) * givenTime)
    },
    "canvasCircleForHours": function (givenTime) {
        return ((6.28 / 24) * givenTime)
    }
};

const viewer = {
    "render": function () {
        this.startRenderingInterval();
    },
    "renderingInterval": 0,
    "startRenderingInterval": function () {
        this.renderingInterval = setInterval(this.updateTime, 1000);
    },
    "updateTime": function () {
        operator.putTimeLeftToCharts();
        viewer.updateCanvas();
    },
    "updateCanvas": function () {
        var lineWidth = model.chartProperties.lineWidth;
        var ctxCanvasForSec = viewer.createCanvas(model.charts.seconds);
        var ctxCanvasForMin = viewer.createCanvas(model.charts.minutes);
        var ctxCanvasForHours = viewer.createCanvas(model.charts.hours);
        var timeForCanvasForSec = operator.canvasCircleForSecAndMin(operator.secondsInAMinuteLeft());
        var timeForCanvasForMin = operator.canvasCircleForSecAndMin(operator.minutesInAnHourLeft());
        var timeForCanvasForHours = operator.canvasCircleForHours(operator.hoursInADayLeft());
        viewer.drawCircle('#179D5E', lineWidth, timeForCanvasForSec, 100, ctxCanvasForSec);
        viewer.drawCircle('#D10023', lineWidth, timeForCanvasForMin, 100, ctxCanvasForMin);
        viewer.drawCircle('#D10023', lineWidth, timeForCanvasForHours, 100, ctxCanvasForHours);
    },
    "createCanvas": function (givenChart) {
        var el = givenChart;
        var canvas = document.createElement('canvas');
        if (typeof(G_vmlCanvasManager) !== 'undefined') {
            G_vmlCanvasManager.initElement(canvas);
        }
        var ctx = canvas.getContext('2d');
        canvas.width = canvas.height = model.chartProperties.diameter;
        el.appendChild(canvas);
        return ctx;
    },
    "drawCircle": function (color, lineWidth, time, percent, ctx) {
        ctx.translate(model.chartProperties.diameter / 2, model.chartProperties.diameter / 2); // change center - szerokosc wysokosc czyli ustawione na środek
        ctx.rotate((-1 / 2 + model.chartProperties.rotate / 180) * Math.PI); // rotate -90 deg
        var radius = (model.chartProperties.diameter - model.chartProperties.lineWidth) / 2;
        percent = Math.min(Math.max(0, percent || 1), 1);
        ctx.beginPath();
        ctx.arc(0, 0, radius, time, Math.PI * 2 * percent, true);
        ctx.strokeStyle = color;
        ctx.lineCap = 'round'; // butt, round or square
        ctx.lineWidth = lineWidth;
        ctx.stroke();
    }
};

(function () {
    viewer.render();
})();